export default class ClassListItemModel {
    public start: Date;
    public end: Date;
    public name: string;
    public location: string;
    
    static parseDateTimeString(dtString: string): Date {
        const parsed = dtString.match(/(\d{4})(\d{2})(\d{2})T(\d{2})(\d{2})(\d{2})/);
        if (parsed) {
            return new Date(
                parseInt(parsed[1]), parseInt(parsed[2]) - 1, parseInt(parsed[3]),
                parseInt(parsed[4]), parseInt(parsed[5]), parseInt(parsed[6])
            );
        } else {
            return new Date();
        }
    }
    static fromICalText(text: string): ClassListItemModel[] {
        const events = text.match(/BEGIN:VEVENT([\s\S]*?)END:VEVENT/g);
        if (events) {
            return events
                .map(item => item
                    .split("\n")
                    .slice(1,-1)
                    .map(item2 => item2.trim().split(":"))
                )
                .map(item => {
                    const classListItem = new ClassListItemModel();
                    for (const column of item) {
                        switch (column[0]) {
                            case "SUMMARY":
                                classListItem.name = column[1]; break;
                            case "LOCATION":
                                classListItem.location = column[1]; break;
                            case "DTSTART;TZID=Asia/Tokyo":
                                classListItem.start = ClassListItemModel.parseDateTimeString(column[1]); break;
                            case "DTEND;TZID=Asia/Tokyo":
                                classListItem.end = ClassListItemModel.parseDateTimeString(column[1]); break;
                        }
                    }
                    return classListItem;
                });
        } else {
            throw new Error("Calendar parse failed.");
        }

    }
}
