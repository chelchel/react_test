import * as React from "react";
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from "@material-ui/core";

export default class AboutDialog extends React.Component<{open: boolean, onClose: () => any}> {
    public render() {
        return (
            <Dialog open={this.props.open} onClose={this.props.onClose}>
                <DialogTitle>About</DialogTitle>
                <DialogContent>
                    <DialogContentText>このアプリはReactの学習用に作成されました。Apache Cordova + React + Material-UI + TypeScriptの構成になっています。</DialogContentText>
                </DialogContent>
                <DialogActions><Button onClick={this.props.onClose}>OK</Button></DialogActions>
            </Dialog>
        );
    }
}