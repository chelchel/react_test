export function zeroFill(num: number, digit: number) {
    return (Array(digit + 1).join("0") + num).slice(-digit);
}