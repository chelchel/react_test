import * as React from "react";
import * as ReactDOM from "react-dom";
import { Typography, AppBar, Toolbar, IconButton, Drawer, Divider, List, ListItem, ListItemText, ListItemIcon } from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import SettingIcon from "@material-ui/icons/Settings"
import InfoIcon from "@material-ui/icons/Info"

import { ClassList } from "./class-list";
import SettingsDialog from "./settings-dialog";
import AboutDialog from "./about-dialog";
import DatePicker from "./date-picker";

interface AppState {
    date: Date;
    calendarUrl: string;
    isDrawerOpen: boolean;
    isSettingsDialogOpen: boolean;
    isAboutDialogOpen: boolean;
}
class App extends React.Component<{}, AppState> {
    constructor() {
        super({});
        const savedCalendarUrl = localStorage.getItem("calendarUrl");
        this.state = {
            date: new Date(),
            calendarUrl: (savedCalendarUrl) ? savedCalendarUrl : "",
            isDrawerOpen: false,
            isSettingsDialogOpen: false,
            isAboutDialogOpen: false
        }
    }
    public handleDateChange() {}
    public render() {
        return (
            <div>
                <AppBar position="sticky">
                    <Toolbar>
                        <IconButton color="inherit" aria-label="Menu" onClick={() => {this.setState({isDrawerOpen: true})}}>
                            <MenuIcon />                    
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{flexGrow: 1}}>
                            {this.state.date.getMonth() + 1}/{this.state.date.getDate()}の時間割
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Toolbar>
                    <IconButton color="inherit" onClick={() => {this.setState({date: new Date(this.state.date.getTime() - 1000 * 60 * 60 * 24)})}}>
                        <ArrowBackIcon />
                    </IconButton>
                    <DatePicker value={this.state.date} onChange={(e) => {this.setState({date: e})}} />
                    <IconButton color="inherit" onClick={() => {this.setState({date: new Date(this.state.date.getTime() + 1000 * 60 * 60 * 24)})}}>
                        <ArrowForwardIcon />
                    </IconButton>
                </Toolbar>
                <ClassList date={this.state.date} calenderUrl={this.state.calendarUrl} />
                <Drawer open={this.state.isDrawerOpen} onClose={() => {this.setState({isDrawerOpen: false})}}>
                    <List>
                        <ListItem button onClick={() => this.setState({isSettingsDialogOpen: true, isDrawerOpen: false})}>
                            <ListItemIcon><SettingIcon /></ListItemIcon>
                            <ListItemText>設定</ListItemText>
                        </ListItem>
                    </List>
                    <Divider />
                    <List>
                        <ListItem button onClick={() => {this.setState({isAboutDialogOpen: true, isDrawerOpen: false})}}>
                            <ListItemIcon><InfoIcon /></ListItemIcon>
                            <ListItemText>このアプリについて</ListItemText>
                        </ListItem>
                    </List>
                </Drawer>
                <SettingsDialog
                    open={this.state.isSettingsDialogOpen}
                    calendarURL={this.state.calendarUrl}
                    onEnter={(newUrl) => {this.setState({calendarUrl: newUrl})}}
                    onClose={() => {this.setState({isSettingsDialogOpen: false})}}
                />
                <AboutDialog open={this.state.isAboutDialogOpen} onClose={() => {this.setState({isAboutDialogOpen: false})}} />
            </div>
        );
    }
}

document.addEventListener("deviceready", () => {
    ReactDOM.render(<App />, document.getElementById("app"));
});