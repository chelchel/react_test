import * as React from "react";
import Card from "@material-ui/core/Card";
import { CardContent, Typography } from "@material-ui/core";

import ClassListItemModel from "./class-list-item-model";
import { zeroFill } from "./lib";

export default class ClassListItem extends React.Component<{data: ClassListItemModel}> {
    private getTimeString() {
        const startHour = this.props.data.start.getHours(), startMinute = this.props.data.start.getMinutes();
        const endHour = this.props.data.end.getHours(), endMinute = this.props.data.end.getMinutes();
        if (startHour == 8 && startMinute == 30 && endHour == 10 && endMinute == 15) {
            return "１限（8:30〜10:15）";
        } else if (startHour == 10 && startMinute == 25 && endHour == 12 && endMinute == 10) {
            return "２限（10:25〜12:10）";
        } else if (startHour == 13 && startMinute == 0  && endHour == 14 && endMinute == 45) {
            return "３限（13:00〜14:45）";
        } else if (startHour == 14 && startMinute == 55 && endHour == 16 && endMinute == 40) {
            return "４限（14:55〜16:40）";
        } else if (startHour == 16 && startMinute == 50 && endHour == 18 && endMinute == 35) {
            return "５限（16:50〜18:35）";
        } else if (startHour == 18 && startMinute == 45 && endHour == 20 && endMinute == 30) {
            return "６限（18:45〜20:30）";
        } else {
            return `特殊（${zeroFill(startHour, 2)}:${zeroFill(startMinute, 2)}〜` +
                `${zeroFill(endHour, 2)}:${zeroFill(endMinute, 2)}）`;
        }
    }
    public render() {
        return (
            <Card>
                <CardContent>
                    <Typography variant="subtitle1" component="time">{this.getTimeString()}</Typography>
                    <Typography variant="h5" component="h2" color="primary">{this.props.data.name}</Typography>
                    <Typography component="p">{this.props.data.location}</Typography>
                </CardContent>
            </Card>
        );
    }
}