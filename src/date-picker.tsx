import * as React from "react";
import { TextField } from "@material-ui/core";
import { zeroFill } from "./lib";

export default class DatePicker extends React.Component<{value: Date, onChange: (value: Date) => any}> {
    private handleChange(e: React.ChangeEvent<HTMLInputElement>) {
        const parsed = e.target.value.match(/(\d{4})-(\d\d?)-(\d\d?)/);
        if (parsed) {
            this.props.onChange(new Date(parseInt(parsed[1]), parseInt(parsed[2]) - 1, parseInt(parsed[3])));
        }
    }
    public render() {
        return (
            <TextField
                type="date"
                value={`${this.props.value.getFullYear()}-${zeroFill(this.props.value.getMonth() + 1, 2)}-${zeroFill(this.props.value.getDate(), 2)}`}
                onChange={this.handleChange.bind(this)}
                style={{flexGrow: 1}}
            />
        );
    }
}