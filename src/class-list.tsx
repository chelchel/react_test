import * as React from "react";
import { Typography } from "@material-ui/core";

import ClassListItemModel from "./class-list-item-model";
import ClassListItem from "./class-list-item";
import { Ajax } from "./ajax";

enum ClassListLoadingProgress {
    UrlNotSpecified, Loading, Loaded, LoadError
}
interface ClassListProps {
    readonly calenderUrl: string;
    date: Date;
}
interface ClassListState {
    classes: ClassListItemModel[],
    progress: ClassListLoadingProgress
}
export class ClassList extends React.Component<ClassListProps, ClassListState> {
    public state: ClassListState = {classes: [], progress: ClassListLoadingProgress.Loading};
    private fetchData() {
        if (this.props.calenderUrl != "") {
            this.setState({progress: ClassListLoadingProgress.Loading});
            Ajax.httpGet(this.props.calenderUrl, (responseText) => {
                try {
                    this.setState({
                        classes: ClassListItemModel.fromICalText(responseText),
                        progress: ClassListLoadingProgress.Loaded
                    });
                } catch (e) {
                    this.setState({progress: ClassListLoadingProgress.LoadError});
                }
            }, () => {
                this.setState({progress: ClassListLoadingProgress.LoadError});
            });
        } else {
            this.setState({progress: ClassListLoadingProgress.UrlNotSpecified});
        }
    }
    public componentDidMount() {
        this.fetchData();
    }
    public componentDidUpdate(prevProps: ClassListProps) {
        if (this.props.calenderUrl !== prevProps.calenderUrl) {
            this.fetchData();
        }
    }
    public render() {
        switch (this.state.progress) {
            case ClassListLoadingProgress.Loaded:
                const showedItems = this.state.classes.filter(v =>
                    v.start.getDate() === this.props.date.getDate() &&
                    v.start.getMonth() === this.props.date.getMonth()
                );
                if (showedItems.length) {
                    return (
                        <ul style={{margin: 0, padding: 0, listStyleType: "none"}}>
                            {showedItems.sort((v1, v2) => v1.start.getTime() - v2.start.getTime()).map((v, index) =>
                                <li style={{margin: "10px"}} key={index}><ClassListItem data={v} /></li>
                            )}
                        </ul>
                    );
                } else {
                    return <Typography style={{margin: "5px"}}>授業がありません</Typography>
                }
            case ClassListLoadingProgress.UrlNotSpecified:
                return <Typography style={{margin: "5px"}}>カレンダーのURLが指定されていません。メニューの「設定」から指定してください。</Typography>
            case ClassListLoadingProgress.Loading:
                return <Typography style={{margin: "5px"}}>読み込み中です…</Typography>
            case ClassListLoadingProgress.LoadError:
                return <Typography style={{margin: "5px"}}>読み込み中にエラーが発生しました。ネットワーク環境もしくはカレンダーのURLを確認してください。</Typography>
        }
    }
}
