export module Ajax {
    export function httpGet(url: string, callback: (responseText: string) => any, error = () => {}) {
        const xhr = new XMLHttpRequest();
        xhr.onloadend = () => {
            if (xhr.status == 200 || xhr.status == 304) {
                callback(xhr.responseText);
            } else {
                error();
            }
        };
        xhr.open("GET", url);
        xhr.send();
    }
}