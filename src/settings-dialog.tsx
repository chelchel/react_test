import * as React from "react";
import { Dialog, DialogTitle, DialogContent, DialogContentText, TextField, DialogActions, Button } from "@material-ui/core";

interface SettingsDialogProps {
    open: boolean;
    calendarURL: string;
    onEnter: (newUrl: string) => any;
    onClose: () => any;
}
export default class SettingsDialog extends React.Component<SettingsDialogProps, {calendarURL: string}> {
    constructor(props: SettingsDialogProps) {
        super(props);
        this.state = {calendarURL: props.calendarURL};
    }
    private savePropertiesHandler() {
        localStorage.setItem("calendarUrl", this.state.calendarURL);
        this.props.onEnter(this.state.calendarURL);
        this.props.onClose();
    }
    public render() {
        return (
            <Dialog open={this.props.open} onClose={this.props.onClose}>
                <DialogTitle>設定</DialogTitle>
                <DialogContent>
                    <DialogContentText>UTASに表示されるカレンダー連携のURLを入力してください。</DialogContentText>
                    <TextField value={this.state.calendarURL} label="URL" fullWidth autoFocus onChange={(e) => {this.setState({calendarURL: e.target.value})}} />
                </DialogContent>
                <DialogActions>
                    <Button color="primary" onClick={this.savePropertiesHandler.bind(this)}>保存</Button>
                    <Button onClick={this.props.onClose}>キャンセル</Button>
                </DialogActions>
            </Dialog>
        );
    }
}