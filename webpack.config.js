module.exports = {
    // モード値を production に設定すると最適化された状態で、
    // development に設定するとソースマップ有効でJSファイルが出力される
    mode: "development",
    entry: __dirname + "/src/main.tsx",
    output: {
        path: __dirname + "/www",
        filename: "bundle.js",
        devtoolModuleFilenameTemplate: "[absolute-resource-path]"
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader"
            }
        ]
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"]
    },
    node: {global: true} // ライブラリ側でnodeモジュールを使っていることで生じるエラーを回避する
};