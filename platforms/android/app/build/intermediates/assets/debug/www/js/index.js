document.addEventListener("deviceready", () => {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 304)) {
            document.getElementById("test").innerHTML = xhr.responseText;
        }
    }
    xhr.open("GET", "http://jishukan.net/");
    xhr.send();
});